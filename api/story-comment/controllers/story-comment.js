'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const {yup, formatYupErrors} = require('strapi-utils');

module.exports = {
  async create(ctx) {
    const mongodbInstance = strapi.connections.default;

    const {body} = ctx.request;

    try {
      await validateCreateCommentSchemaInput(body);
    } catch (err) {
      return ctx.badRequest('ValidationError', err);
    }


    const {text, storyId: story, replyTo} = body;
    const newComment = await strapi.services['story-comment'].create({
      text,
      story,
      author: ctx.state.user.id,
      parentComment: replyTo,
    });

    if (replyTo) {
      const sourceComment = await strapi.services['story-comment'].findOne({id: replyTo});

      const answers = sourceComment.answers || [];
      answers.push(newComment.id);

      await strapi.services['story-comment'].update(
        {_id: sourceComment.id},
        {answers},
        {"upsert": false}
      );
    }

    return newComment;
    //
    // await mongodbInstance.models.StoryComment.insetOne(
    //   { _id: { $in: ids } },
    //   {status: status},
    //   { "upsert": false }
    // // );
    //

    // return sanitizeEntity(entity, { model: strapi.models.restaurant });

    // return body;
  },
};

const createCommentSchema = yup
  .object()
  .shape({
    text: yup.string().required(),
    storyId: yup.string().required(),
    replyTo: yup.string(),
  })
  .required()
  .noUnknown();

const validateCreateCommentSchemaInput = data => {
  return createCommentSchema
    .validate(data, {strict: true, abortEarly: false})
    .catch(error => Promise.reject(formatYupErrors(error)));
};
