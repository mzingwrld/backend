'use strict';

const {sanitizeEntity} = require('strapi-utils');
const {AUTHOR, EDITOR, ADMIN} = require("../../../roles");
const {yup, formatYupErrors} = require('strapi-utils');
const {filter, each, map} = require('lodash');
const dateAndTime = require('date-and-time');
const mongodb = require("mongodb");
const BSONRegExp = mongodb.BSONRegExp;

const StoryStatus = require("../../../storyStatus");
const {
  DRAFT, REJECTED, WAIT_FOR_APPROVE, APPROVED, PAYMENT_IN_PROGRESS, PAID, WAIT_PROOFREADING_OF_PAID,
  IN_PROOFREADING_OF_PAID, PAYMENT_ERROR, TO_BUILD, IN_BUILD, PROOFREADING_OF_PAID_DONE, TRASH, FOR_DELETION,
} = StoryStatus;
const addWhereByRole = require("../../../addWhereByRole");
const {ObjectId} = require("bson");
const {TEMPLATE} = require("../../../storyStatus");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const checkAccess = (entity, ctx) => {
  if (entity && entity.status) {
    if (
      ctx.state.user.role.name === AUTHOR
      && (
        ![DRAFT, REJECTED, WAIT_FOR_APPROVE].includes(entity.status)
        || entity.author.id !== ctx.state.user.id
      )
    ) {
      ctx.forbidden(`Not allowed`);
    }

    if (
      ctx.state.user.role.name === EDITOR
      && ![
        WAIT_FOR_APPROVE,
        APPROVED,
        REJECTED,
        WAIT_PROOFREADING_OF_PAID,
        IN_PROOFREADING_OF_PAID,
        PROOFREADING_OF_PAID_DONE,
        TRASH,
        FOR_DELETION,
      ].includes(entity.status)
    ) {
      ctx.forbidden(`Not allowed`);
    }

    if (
      ctx.state.user.role.name === EDITOR
      && !ctx.state.user.canTakeAnyStory
      && (
        !entity.editor
        || ctx.state.user.id !== entity.editor.id
      )
    ) {
      ctx.forbidden(`Not allowed`);
    }

    if (
      ctx.state.user.role.name === ADMIN
      && entity.status === DRAFT
    ) {
      ctx.forbidden(`Not allowed`);
    }
  }
}

const addAuthorRoleWhere = (q = []) => {
  const {_where = [], ...rest} = q;
  let newWhere = {'role.name': AUTHOR};

  if (q && q._where) {
    if (Array.isArray(q._where)) {
      return {
        _where: [
          ..._where,
          newWhere,
        ],
        ...rest,
      };
    }

    return {
      _where: [
        _where,
        newWhere,
      ],
      ...rest,
    };
  }

  return {
    _where: newWhere,
    ...rest,
  };
}

const addEditorRoleWhere = (q = []) => {
  const {_where = [], ...rest} = q;
  let newWhere = {'role.name': EDITOR};

  if (q && q._where) {
    if (Array.isArray(q._where)) {
      return {
        _where: [
          ..._where,
          newWhere,
        ],
        ...rest,
      };
    }

    return {
      _where: [
        _where,
        newWhere,
      ],
      ...rest,
    };
  }

  return {
    _where: newWhere,
    ...rest,
  };
}

const sanitizeUser = user =>
  sanitizeEntity(user, {
    model: strapi.query('user', 'users-permissions').model,
  });

const findStoriesWithText = async (findText, category, qq, isCount = false) => {
  const mongodbInstance = strapi.connections.default;

  const pattern = `.*${findText}.*`;
  const findQuery = {
    "$and": [
      {
        "$or": [
          {
            "stringId": {'$regex': pattern, '$options': 'i'}
          },
          // {
          //   "templateName": {'$regex': findText, '$options': 'i'}
          // },
          {
            "formData.storyText": new BSONRegExp(pattern, "i")
          },
          {
            "formData.question": new BSONRegExp(pattern, "i")
          },
          {
            "formData.options": {
              "$elemMatch": {
                "option": new BSONRegExp(pattern, "i")
              }
            }
          },
          {
            "formData.options": {
              "$elemMatch": {
                "result": new BSONRegExp(pattern, "i")
              }
            }
          },
          {
            "formData.options": {
              "$elemMatch": {
                "diaryEntry": new BSONRegExp(pattern, "i")
              }
            }
          }
        ]
      }
    ],
  };

  if (category) {
    const categoryQuery = {};
    if (category[0] === '__EMPTY__') {
      categoryQuery["formData.category"] = "";
    } else {
      if (category[0]) {
        categoryQuery["formData.category.0"] = category[0];
      }
      if (category[1]) {
        categoryQuery["formData.category.1"] = category[1];
      }
      if (category[2]) {
        categoryQuery["formData.category.2"] = category[2];
      }
    }

    if (Object.keys(categoryQuery).length) {
      findQuery["$and"].push(categoryQuery);
    }
  }

  if (isCount) {
    let storiesCursor = mongodbInstance.models
      .Story
      .find(
        findQuery
      );

    return await storiesCursor.count();
  }

  const sort = [["proofreadingFinishDate", 1.0], ['authorFinishDate', 1.0], ['createdAt', 1.0]];

  let storiesCursor = mongodbInstance.models
    .Story
    .find(
      findQuery
    )
    .sort(sort);

  if (qq._start) {
    storiesCursor.skip(parseInt(qq._start, 10));
  }

  if (qq._limit) {
    storiesCursor.limit(parseInt(qq._limit, 10));
  }

  return storiesCursor;
}

module.exports = {
  async findOne(ctx) {
    const {id} = ctx.params;

    const entity = await strapi.services.story.findOne({id});
    checkAccess(entity, ctx);

    return sanitizeEntity(entity, {model: strapi.models.story});
  },
  async find(ctx) {
    // const stories1 = await strapi.services.story.find();
    // each(stories1, story => {
    //   strapi.services.story.update({id: story.id}, {stringId: story.id.toString()});
    // });

    const {query} = ctx;

    const {storyTextContain, category, ...restQuery} = query;

    const qq = addWhereByRole(restQuery, ctx);

    const findText = storyTextContain ? String(storyTextContain).trim().toLowerCase() : '';

    if (!findText && !category) {
      const entities = await strapi.services.story.find(qq);

      return entities.map(entity => sanitizeEntity(entity, {model: strapi.models.story}));
    }

    const stories = await findStoriesWithText(findText, category, qq);

    const ids = map(stories, story => story._id);

    if (ids.length === 0) {
      return [];
    }

    qq['_where'].push({
      _or: {
        _id: ids
      }
    });

    const entities = await strapi.services.story.find(qq);

    return entities.map(entity => sanitizeEntity(entity, {model: strapi.models.story}));
  },
  async authors(ctx) {
    const qq = addAuthorRoleWhere(ctx.query);

    const entities = await strapi.query('user', 'users-permissions').search(qq);

    return entities.map(entity => sanitizeUser(entity));
  },
  async editors(ctx) {
    const qq = addEditorRoleWhere(ctx.query);

    const entities = await strapi.query('user', 'users-permissions').search(qq);

    return entities.map(entity => sanitizeUser(entity));
  },
  async count(ctx) {
    const {query} = ctx;

    const {storyTextContain, category, ...restQuery} = query;

    const qq = addWhereByRole(restQuery, ctx);

    const findText = storyTextContain ? String(storyTextContain).trim().toLowerCase() : '';

    if (!findText && !category) {
      return await strapi.services.story.count(qq);
    }

    const stories = await findStoriesWithText(findText, category, qq);

    const ids = map(stories, story => story._id);

    if (ids.length === 0) {
      return [];
    }

    qq['_where'].push({
      _or: {
        _id: ids
      }
    });

    return await strapi.services.story.count(qq);
  },
  async bulkStatusChange(ctx) {
    const mongodbInstance = strapi.connections.default;
    const {body} = ctx.request;

    try {
      const res = await validateBulkStatusChangeInput(body);
    } catch (err) {
      return ctx.badRequest('ValidationError', err);
    }
    const {ids, status} = body;

    await mongodbInstance.models.Story.updateMany(
      {_id: {$in: ids}},
      {status: status},
      {"upsert": false}
    );

    return {
      'success': true,
    };
  },
  async update(ctx) {
    const {id} = ctx.params;

    const story = await strapi.services.story.findOne({id});

    const jsonIsObject = typeof story.data === 'object';
    const originalJson = jsonIsObject ? story.data : JSON.parse(story.data);

    const originalJsonDescription = originalJson.Description ? originalJson.Description : {};
    const newJson = JSON.parse(ctx.request.body.data);

    const {Description, ...restJson} = newJson;

    const json = {
      Description: originalJsonDescription,
      ...restJson,
    };

    const body = {
      ...ctx.request.body,
      data: JSON.stringify(json, null, 2),
    }

    let entity = await strapi.services.story.update({id}, body);

    if (ctx.state.user.role.name === AUTHOR) {
      const statusBefore = story.status;
      const statusAfter = entity.status;

      if ((statusBefore === DRAFT || statusBefore === REJECTED) && statusAfter === WAIT_FOR_APPROVE) {
        const jsonIsObject = typeof entity.data === 'object';
        const newJson = jsonIsObject ? entity.data : JSON.parse(entity.data);
        const {Description = {}, ...restJson} = newJson;

        const finishedAt = dateAndTime.format(new Date(), 'DD.MM.YYYY HH:mm:ss');

        const json = {
          Description: {
            ...Description,
            TimeCreated: finishedAt,
          },
          ...restJson,
        };

        entity = await strapi.services.story.update({id}, {
          authorFinishDate: new Date(),
          data: JSON.stringify(json, null, 2),
        });
      }
    }

    if (ctx.state.user.role.name === EDITOR) {
      const statusBefore = story.status;
      const statusAfter = entity.status;

      if (
        (statusAfter !== statusBefore)
        && (statusAfter === PROOFREADING_OF_PAID_DONE || statusAfter === APPROVED)
      ) {
        // Была произведена вычитка
        entity = await strapi.services.story.update({id}, {
          proofreadingFinishDate: new Date(),
          editor: ctx.state.user.id,
        });

        entity = await setStoryEditorInDescription(ctx, entity);
      }

      if (!story.fixed) {
        const formDataBefore = JSON.stringify(story.formData);
        const formDataAfter = JSON.stringify(entity.formData);

        const haveChanges = formDataBefore !== formDataAfter;

        if (haveChanges) {
          entity = await strapi.services.story.update({id}, {
            fixed: true,
          });
        }
      }
    }

    if (!ctx.request.body.template) {
      entity = await strapi.services.story.update({id}, {
        template: null,
      });
    }

    return sanitizeEntity(entity, {model: strapi.models.story});
  },
  async create(ctx) {
    const entity = await strapi.services.story.create(ctx.request.body);

    const authorName = ctx.state.user.username;
    const createdAt = dateAndTime.format(new Date(), 'DD.MM.YYYY HH:mm:ss');

    const jsonIsObject = typeof entity.data === 'object';
    const json = jsonIsObject ? entity.data : JSON.parse(entity.data);

    const oldDescription = json.Description ? json.Description : {};

    const newDescription = {
      ...oldDescription,
      TimeCreated: createdAt,
      Author: authorName,
    }

    const newJson = {
      Description: newDescription,
      ...json,
    }

    const entityAfter = await strapi.services.story.update({id: entity.id}, {
      data: JSON.stringify(newJson, null, 2),
    });

    return sanitizeEntity(entityAfter, {model: strapi.models.story});
  },
  async appoint(ctx) {
    if (ctx.state.user.role.name !== ADMIN) {
      ctx.forbidden(`Not allowed`);
      return;
    }

    const {storiesIds, editorId} = ctx.request.body;

    const mongodbInstance = strapi.connections.default;
    await mongodbInstance.models.Story.updateMany(
      {_id: {$in: storiesIds}},
      {editor: new ObjectId(editorId)},
      {"upsert": false}
    );

    return {
      status: 'ok',
    }
  },
  async bulkTemplatesAvailableChange(ctx) {
    const {storiesIds, available} = ctx.request.body;

    const mongodbInstance = strapi.connections.default;
    await mongodbInstance.models.Story.updateMany(
      {_id: {$in: storiesIds}, status: TEMPLATE},
      {available: Boolean(available)},
      {"upsert": false}
    );

    return {
      status: 'ok',
    }
  },
  async availableTemplates(ctx) {
    const entities = await strapi.services.story.find({
      status: TEMPLATE,
      available: true,
    });

    return sanitizeEntity(entities, {model: strapi.models.story});
  },
  async paymentExcel(ctx) {
    const entities = await strapi.services.story.find({
      status: PAYMENT_IN_PROGRESS,
      _limit: -1,
    });

    const authors = {};
    const editors = {};

    if (entities.length === 0) {
      return {
        authors,
        editors,
      }
    }

    entities.forEach((story) => {
      if (!story.author || !story.author.id) {
        return;
      }

      const record = authors[story.author.id] || {};
      const count = record.count ? record.count : 0;

      record.author = story.author.username;
      record.creditCardNumber = story.author.creditCardNumber || '';
      record.creditCardOwnerName = story.author.creditCardOwnerName || '';
      record.paypall = story.author.paypall || '';
      record.count = count + 1;
      authors[story.author.id] = record;
    });

    entities.forEach((story) => {
      if (!story.editor || !story.editor.id) {
        return;
      }

      const record = editors[story.editor.id] || {};
      const count = record.count ? record.count : 0;

      record.editor = story.editor.username;
      record.creditCardNumber = story.editor.creditCardNumber || '';
      record.creditCardOwnerName = story.editor.creditCardOwnerName || '';
      record.paypall = story.editor.paypall || '';
      record.count = count + 1;
      editors[story.editor.id] = record;
    });

    return {
      authors,
      editors,
    }
  }
};

const setStoryEditorInDescription = async (ctx, entity) => {
  const editorName = ctx.state.user.username;
  const timeProofreading = dateAndTime.format(new Date(), 'DD.MM.YYYY HH:mm:ss');

  const json = JSON.parse(entity.data, null, 2);

  const oldDescription = json.Description ? json.Description : {};

  const newDescription = {
    ...oldDescription,
    TimeProofreading: timeProofreading,
    Editor: editorName,
  }

  const newJson = {
    ...json,
    Description: newDescription,
  }

  return await strapi.services.story.update({id: entity.id}, {
    data: JSON.stringify(newJson, null, 2),
  });
}

const bulkStatusChangeSchema = yup
  .object()
  .shape({
    status: yup.string().oneOf(Object.values(StoryStatus)).required(),
    ids: yup.array().of(yup.string().required()).required(),
  })
  .required()
  .noUnknown();

const validateBulkStatusChangeInput = data => {
  return bulkStatusChangeSchema
    .validate(data, {strict: true, abortEarly: false})
    .catch(error => Promise.reject(formatYupErrors(error)));
};

