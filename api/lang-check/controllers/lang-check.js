'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const axios = require("axios");
const {sanitizeEntity} = require("strapi-utils");
const qs = require('qs');
const {filter} = require('lodash');


const languagetoolApi = {
  check: async (params, premium = false) => {
    let requestParams = params;

    if (premium) {
      const username = process.env.LANG_USERNAME || '';
      const apiKey = process.env.LANG_APIKEY || '';
      requestParams = {
        ...requestParams,
        username,
        apiKey
      }
    }

    const data = qs.stringify(requestParams);
    const config = {
      method: 'post',
      url: 'https://api.languagetoolplus.com/v2/check',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      },
      data : data
    };

    return axios(config);
  }
}

const promiseCheck = (params, premium = false) => new Promise(function (resolve, reject) {
  try {
    languagetoolApi
      .check(params, premium)
      .then(res => {
        if (Array.isArray(res.data.matches)) {
          const results = filter(res.data.matches, item => {
            return item.rule.issueType === 'grammar' || item.rule.issueType === 'misspelling';
          });
          resolve(results);
        } else {
          resolve([]);
        }
      })
      .catch((err) => {
        console.error('err', err.response.data);
        reject(err);
      });
  } catch (error) {
    console.error(error);
    reject(error);
  }
});

module.exports = {
  async check(ctx) {
    const {text} = ctx.params;

    const existedPremiumEntity = await strapi.services['lang-check'].findOne({
      language: "en-US",
      text: text,
      premium: true,
    });

    if (existedPremiumEntity) {
      return sanitizeEntity(existedPremiumEntity, {model: strapi.models['lang-check']});
    }

    const existedNonPremiumEntity = await strapi.services['lang-check'].findOne({
      language: "en-US",
      text: text,
      premium: false,
    });

    if (existedNonPremiumEntity) {
      return sanitizeEntity(existedNonPremiumEntity, {model: strapi.models['lang-check']});
    }

    const params = {
      language: "en-US",
      text: text,
    };

    const matches = await promiseCheck(params, false);

    const entity = await strapi.services['lang-check'].create({
      language: "en-US",
      text: text,
      premium: false,
      matches,
    });

    return sanitizeEntity(entity, {model: strapi.models['lang-check']});
  },
  async checkPremium(ctx) {
    const {text} = ctx.params;

    const existedPremiumEntity = await strapi.services['lang-check'].findOne({
      language: "en-US",
      text: text,
      premium: true,
    });

    if (existedPremiumEntity) {
      return sanitizeEntity(existedPremiumEntity, {model: strapi.models['lang-check']});
    }

    const params = {
      language: "en-US",
      text: text,
    };

    const matches = await promiseCheck(params, true);

    const entity = await strapi.services['lang-check'].create({
      language: "en-US",
      text: text,
      premium: true,
      matches,
    });

    return sanitizeEntity(entity, {model: strapi.models['lang-check']});
  },
};
