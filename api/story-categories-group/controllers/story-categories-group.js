'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const {sanitizeEntity} = require("strapi-utils");

module.exports = {
  async find(ctx) {
    const entities = await strapi
      .query('story-categories-group')
      .find(ctx.query, [
        {
          path: "story_categories_sub_groups",
          populate: {
            path: "story_categories"
          }
        }
      ]);

    return entities.map(entity => sanitizeEntity(entity, {model: strapi.models['story-categories-group']}));
  },
};
