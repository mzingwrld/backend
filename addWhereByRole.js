const {AUTHOR, EDITOR, ADMIN} = require("./roles");
const StoryStatus = require("./storyStatus");
const {
  DRAFT, REJECTED, WAIT_FOR_APPROVE, APPROVED, PAYMENT_IN_PROGRESS, PAID, WAIT_PROOFREADING_OF_PAID,
  IN_PROOFREADING_OF_PAID, PAYMENT_ERROR, TO_BUILD, IN_BUILD, PROOFREADING_OF_PAID_DONE, TEMPLATE, TRASH, FOR_DELETION,
} = StoryStatus;

const addWhereByRole = (q = [], ctx) => {
  const {_where = [], ...rest} = q;
  let newWhere = [];

  if (ctx.state.user.role.name === AUTHOR) {
    const userId = ctx.state.user.id;

    newWhere = [
      {author: userId},
      {
        _or: [
          {status: DRAFT},
          {status: REJECTED},
          {status: WAIT_FOR_APPROVE},
        ],
      }
    ];
  }


  if (ctx.state.user.role.name === EDITOR) {
    newWhere = [
      {
        _or: [
          {status: WAIT_FOR_APPROVE},
          {status: APPROVED},
          {status: REJECTED},
          {status: WAIT_PROOFREADING_OF_PAID},
          {status: IN_PROOFREADING_OF_PAID},
          {status: PROOFREADING_OF_PAID_DONE},
          {status: TRASH},
          {status: FOR_DELETION},
        ],
      }
    ];

    if (!ctx.state.user.canTakeAnyStory) {
      const x = {
        editor: ctx.state.user.id,
      }
      newWhere.push(x);
    }
  }

  if (ctx.state.user.role.name === ADMIN) {
    newWhere = [
      {
        _or: [
          {status: WAIT_FOR_APPROVE},
          {status: APPROVED},
          {status: PAYMENT_IN_PROGRESS},
          {status: PAID},
          {status: REJECTED},
          {status: PAYMENT_ERROR},
          {status: TO_BUILD},
          {status: IN_BUILD},
          {status: WAIT_PROOFREADING_OF_PAID},
          {status: IN_PROOFREADING_OF_PAID},
          {status: PROOFREADING_OF_PAID_DONE},
          {status: TEMPLATE},
          {status: TRASH},
          {status: FOR_DELETION},
        ],
      }
    ];
  }

  if (q && q._where) {
    if (Array.isArray(q._where)) {
      return {
        _where: [
          ..._where,
          newWhere,
        ],
        ...rest,
      };
    }

    return {
      _where: [
        _where,
        newWhere,
      ],
      ...rest,
    };
  }

  return {
    _where: newWhere,
    ...rest,
  };
}

module.exports = addWhereByRole;
